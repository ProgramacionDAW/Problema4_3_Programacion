﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Problema4
{
    public partial class Form1 : Form
    {
        Datos datos;
        int registroActual;
        StreamWriter informe;

        /// <summary>
        /// Constructor base
        /// </summary>
        public Form1()
        {
            InitializeComponent();
            textMarca.Location = comboMarca.Location; //Coloco ambos controles en el mismo sitio
            AgregarMarcas();
            datos = new Datos();
            registroActual = 0;
            if (datos.TotalRegistros > 0)
            {
                labelTotal.Text = string.Concat("de ", @"{", datos.TotalRegistros, @"}");
                LeerPrimerVehiculo();
            }
        }

        /// <summary>
        /// Cierra el formulario al hacer click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void opcionSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// Registra un nuevo vehiculo y actualiza el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textMatricula.Text))
            {
                MessageBox.Show("La matrícula no puede estar vacía", "Error", MessageBoxButtons.OK);
                return;
            }
            textMatricula.Text = textMatricula.Text.ToUpper();

            Vehiculo vehiculo;
            vehiculo.matricula = textMatricula.Text;
            vehiculo.marca = comboMarca.Text;
            vehiculo.modelo = textModelo.Text;
            vehiculo.color = textColor.Text;
            datos.Agregar(vehiculo);

            botonGuardar.Visible = false;
            textMarca.Visible = true;
            comboMarca.Visible = false;
            textMatricula.ReadOnly = true;
            textModelo.ReadOnly = true;
            textColor.ReadOnly = true;
            textMatricula.Clear();
            textModelo.Clear();
            textColor.Clear();
            textMarca.Clear();
            HabilitarPanel2();
            labelTotal.Text = string.Concat("de ", @"{", datos.TotalRegistros, @"}");

            if (registroActual == 0)
            {
                textMatricula.Clear();
                textMarca.Clear();
                textModelo.Clear();
                textColor.Clear();
            }
            else
            {
                vehiculo = datos.IraRegistro(registroActual);
                textMatricula.Text = vehiculo.matricula;
                textMarca.Text = vehiculo.marca;
                textModelo.Text = vehiculo.modelo;
                textColor.Text = vehiculo.color;
            }
        }

        /// <summary>
        /// Agrega marcas al comboBox
        /// </summary>
        private void AgregarMarcas()
        {
            string[] marcas = new string[] { "Ford", "Renault", "Audi", "BMW", "Chevrolet", "Citroen", "Fiat" };
            Array.Sort(marcas);
            for (int i=0;i<marcas.Length;i++)
            {
                comboMarca.Items.Add(marcas[i]);
            }
        }

        /// <summary>
        /// Prepara el formulario para agregar un vehiculo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonAgregar_Click(object sender, EventArgs e)
        {
            textMarca.Visible = false;
            comboMarca.Visible = true;
            botonGuardar.Visible = true;
            DeshabilitarPanel();
            textMatricula.Clear();
            textModelo.Clear();
            textColor.Clear();
            textMarca.Clear();
            textMatricula.ReadOnly = false;
            textModelo.ReadOnly = false;
            textColor.ReadOnly = false;
        }

        /// <summary>
        /// Lee el primer vehiculo y lo muestra
        /// </summary>
        private void LeerPrimerVehiculo()
        {
            Vehiculo vehiculo = datos.IraRegistro(1);
            textMatricula.Text = vehiculo.matricula;
            textMarca.Text = vehiculo.marca;
            textModelo.Text = vehiculo.modelo;
            textColor.Text = vehiculo.color;
            labelActual.Text = "1";
            registroActual = 1;
        }

        /// <summary>
        /// Lee el siguiente vehiculo y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonSiguiente_Click(object sender, EventArgs e)
        {
            if (registroActual < datos.TotalRegistros)
            {
                Vehiculo vehiculo = datos.IraRegistro(registroActual + 1);
                textMatricula.Text = vehiculo.matricula;
                textMarca.Text = vehiculo.marca;
                textModelo.Text = vehiculo.modelo;
                textColor.Text = vehiculo.color;
                labelActual.Text = (registroActual + 1).ToString();
                registroActual++;
            }
        }

        /// <summary>
        /// Lee el vehiculo anterior y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonAnterior_Click(object sender, EventArgs e)
        {
            if (registroActual > 1) 
            {
                Vehiculo vehiculo = datos.IraRegistro(registroActual - 1);
                textMatricula.Text = vehiculo.matricula;
                textMarca.Text = vehiculo.marca;
                textModelo.Text = vehiculo.modelo;
                textColor.Text = vehiculo.color;
                labelActual.Text = (registroActual - 1).ToString();
                registroActual--;
            }
        }

        /// <summary>
        /// Lee el primer vehiculo y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonPrimero_Click(object sender, EventArgs e)
        {
            if (registroActual > 1)
            {
                Vehiculo vehiculo = datos.IraRegistro(1);
                textMatricula.Text = vehiculo.matricula;
                textMarca.Text = vehiculo.marca;
                textModelo.Text = vehiculo.modelo;
                textColor.Text = vehiculo.color;
                labelActual.Text = "1";
                registroActual = 1;
            }
        }

        /// <summary>
        /// Lee el ultimo vehiculo y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonUltimo_Click(object sender, EventArgs e)
        {
            if (registroActual < datos.TotalRegistros)
            {
                Vehiculo vehiculo = datos.IraRegistro(datos.TotalRegistros);
                textMatricula.Text = vehiculo.matricula;
                textMarca.Text = vehiculo.marca;
                textModelo.Text = vehiculo.modelo;
                textColor.Text = vehiculo.color;
                labelActual.Text = (datos.TotalRegistros).ToString();
                registroActual = datos.TotalRegistros;
            }
        }

        /// <summary>
        /// Lee el vehiculo seleccionado y lo muestra
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelActual_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar==(char)Keys.Enter)
            {
                int registro;
                if (!int.TryParse(labelActual.Text, out registro))
                {
                    MessageBox.Show("Dato introducido no valido", "Error", MessageBoxButtons.OK);
                    labelActual.Text = registroActual.ToString();
                }
                else if (registro<=0)
                {
                    MessageBox.Show("Dato introducido no valido", "Error", MessageBoxButtons.OK);
                    labelActual.Text = registroActual.ToString();
                }
                else
                {
                    if (registro <= datos.TotalRegistros)
                    {
                        Vehiculo vehiculo = datos.IraRegistro(registro);
                        textMatricula.Text = vehiculo.matricula;
                        textMarca.Text = vehiculo.marca;
                        textModelo.Text = vehiculo.modelo;
                        textColor.Text = vehiculo.color;
                        labelActual.Text = (registro).ToString();
                        registroActual = registro;
                    }
                    else
                    {
                        MessageBox.Show("Registro introducido no valido", "Error", MessageBoxButtons.OK);
                        labelActual.Text = registroActual.ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Deshabilita ciertos controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelActual_Enter(object sender, EventArgs e)
        {
            botonPrimero.Enabled = false;
            botonUltimo.Enabled = false;
            botonSiguiente.Enabled = false;
            botonAnterior.Enabled = false;
            botonAgregar.Enabled = false;
            botonEliminar.Enabled = false;
            botonEditar.Enabled = false;
            registroActual = int.Parse(labelActual.Text);
        }

        /// <summary>
        /// Habilita ciertos controles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void labelActual_Leave(object sender, EventArgs e)
        {
            botonAnterior.Enabled = true;
            botonSiguiente.Enabled = true;
            botonUltimo.Enabled = true;
            botonPrimero.Enabled = true;
            botonAgregar.Enabled = true;
            botonEliminar.Enabled = true;
            botonEditar.Enabled = true;
            int registro;
            if (!int.TryParse(labelActual.Text, out registro))
            {
                labelActual.Text = registroActual.ToString();
            }
            if (registro != registroActual)
            {
                labelActual.Text = registroActual.ToString();
            }
        }

        /// <summary>
        /// Guarda los cambios y edita el vehiculo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonEditar_Click(object sender, EventArgs e)
        {
            if (registroActual>0)
            {
                textMarca.Visible = false;
                comboMarca.Visible = true;
                comboMarca.Text = textMarca.Text;
                textMatricula.ReadOnly = false;
                textModelo.ReadOnly = false;
                textColor.ReadOnly = false;
                botonSave.Visible = true;
                botonDiscard.Visible = true;
                DeshabilitarPanel();
            }
            else
            {
                MessageBox.Show("No se puede editar este registro", "Error", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Cancela los cambios y vuekve al registro actual
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonDiscard_Click(object sender, EventArgs e)
        {
            Vehiculo vehiculo = datos.IraRegistro(registroActual);
            textMatricula.Text = vehiculo.matricula;
            textMarca.Text = vehiculo.marca;
            textModelo.Text = vehiculo.modelo;
            textColor.Text = vehiculo.color;
            botonDiscard.Visible = false;
            botonSave.Visible = false;
            HabilitarPanel();
        }

        /// <summary>
        /// Guarda los cambios y edita el vehiculo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonSave_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textMatricula.Text))
            {
                MessageBox.Show("La matrícula no puede estar vacía", "Error", MessageBoxButtons.OK);
                return;
            }
            Vehiculo vehiculo;
            vehiculo.matricula = textMatricula.Text;
            vehiculo.marca = comboMarca.Text;
            vehiculo.modelo = textModelo.Text;
            vehiculo.color = textColor.Text;
            datos.Actualizar(vehiculo, registroActual);
            botonDiscard.Visible = false;
            botonSave.Visible = false;
            HabilitarPanel();
        }

        /// <summary>
        /// Deshabilita ciertos controles
        /// </summary>
        private void DeshabilitarPanel()
        {
            botonPrimero.Enabled = false;
            botonAnterior.Enabled = false;
            botonSiguiente.Enabled = false;
            botonUltimo.Enabled = false;
            botonAgregar.Enabled = false;
            botonEliminar.Enabled = false;
            botonEditar.Enabled = false;
            labelActual.Enabled = false;
            opcionBuscar.Enabled = false;
        }

        /// <summary>
        /// Habilita ciertos controles
        /// </summary>
        public void HabilitarPanel2()
        {
            botonPrimero.Enabled = true;
            botonAnterior.Enabled = true;
            botonSiguiente.Enabled = true;
            botonUltimo.Enabled = true;
            botonAgregar.Enabled = true;
            botonEliminar.Enabled = true;
            botonEditar.Enabled = true;
            labelActual.Enabled = true;
            opcionBuscar.Enabled = true;
        }

        /// <summary>
        /// Habilita ciertos controles
        /// </summary>
        private void HabilitarPanel()
        {
            botonPrimero.Enabled = true;
            botonAnterior.Enabled = true;
            botonSiguiente.Enabled = true;
            botonUltimo.Enabled = true;
            botonAgregar.Enabled = true;
            botonEliminar.Enabled = true;
            opcionBuscar.Enabled = true;
            botonEditar.Enabled = true;
            labelActual.Enabled = true;
            textMarca.Visible = true;
            comboMarca.Visible = false;
            textMarca.Text = comboMarca.Text;
            textMatricula.ReadOnly = true;
            textModelo.ReadOnly = true;
            textColor.ReadOnly = true;
            botonSave.Visible = false;
            botonDiscard.Visible = false;
        }

        /// <summary>
        /// Habilita la busqueda de matriculas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void opcionBuscar_Click(object sender, EventArgs e)
        {
            string matricula;
            BuscaMatricula form = new BuscaMatricula();
            form.ShowDialog();
            matricula = form.labelMatricula.Text;
            string[] vehiculo = datos.Buscar(matricula);
            if (vehiculo!=null)
            {
                textMatricula.Text = vehiculo[0];
                textMarca.Text = vehiculo[1];
                textModelo.Text = vehiculo[2];
                textColor.Text = vehiculo[3];
                labelActual.Text = vehiculo[4];
                registroActual = int.Parse(vehiculo[4]);
            }
            else
            {
                MessageBox.Show("La matrícula introducida no existe.", "Información", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Elimina un vehiculo
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonEliminar_Click(object sender, EventArgs e)
        {
            if (registroActual>0)
            {
                datos.Eliminar(textMatricula.Text);
                registroActual--;
                if (registroActual == 0)
                {
                    textMatricula.Clear();
                    textMarca.Clear();
                    textModelo.Clear();
                    textColor.Clear();
                }
                else
                {
                    Vehiculo vehiculo = datos.IraRegistro(registroActual);
                    textMatricula.Text = vehiculo.matricula;
                    textMarca.Text = vehiculo.marca;
                    textModelo.Text = vehiculo.modelo;
                    textColor.Text = vehiculo.color;
                }
                labelActual.Text = registroActual.ToString();
                labelTotal.Text = string.Concat("de ", @"{", datos.TotalRegistros, @"}");
            }
            else
            {
                MessageBox.Show("No se puede eliminar este registro", "Error", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// Abre el dialogo de selección de archivos
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void abrirInforme_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    string ruta = openFileDialog1.FileName;
                    informe = new StreamWriter(ruta, false);
                    botonGuardarInfo.Enabled = true;
                }
                catch
                {
                    MessageBox.Show("El archivo seleccionado no se ha podido leer");
                }
            }
        }

        /// <summary>
        /// Guarda los datos en el archivo seleccionado
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonGuardarInfo_Click(object sender, EventArgs e)
        {
            TextWriter escribe = informe;
            datos.EscribirInforme(escribe);
            informe.Close();
            botonGuardarInfo.Enabled = false;
        }
    }
}
