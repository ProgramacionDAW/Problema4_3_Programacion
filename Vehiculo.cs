﻿namespace Problema4
{
    public struct Vehiculo
    {
        public string matricula;
        public string marca;
        public string modelo;
        public string color;
    }
}
