﻿using System;
using System.Windows.Forms;

namespace Problema4
{
    public partial class BuscaMatricula : Form
    {
        /// <summary>
        /// Constructor base
        /// </summary>
        public BuscaMatricula()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Pone el texto introducido en mayusculas y cierra el formulario
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void botonBuscar_Click(object sender, EventArgs e)
        {
            labelMatricula.Text = labelMatricula.Text.ToUpper();
            DialogResult = DialogResult.OK;
        }
    }
}
