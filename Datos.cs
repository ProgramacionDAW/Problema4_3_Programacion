﻿using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;

namespace Problema4
{
    class Datos
    {
        #region Atributos
        SQLiteConnection conexion;
        List<Vehiculo> ListaVehiculos;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor base
        /// </summary>
        public Datos()
        {
            ListaVehiculos = Program.vehiculos;
            if (File.Exists(@"VEHICULOS.fich"))
            {
                conexion = new SQLiteConnection("Data Source=VEHICULOS.fich;Version=3;");
            }
            else
            {
                CrearBaseDatos();
                VehiculosEjemplo();
            }
            LeerBaseDatos();
        }
        #endregion

        #region Métodos
        /// <summary>
        /// Crea una base de datos en caso de que no exista
        /// </summary>
        private void CrearBaseDatos()
        {
            SQLiteConnection.CreateFile(@"VEHICULOS.fich");
            conexion = new SQLiteConnection("Data Source=VEHICULOS.fich;Version=3;");
            conexion.Open();
            string consulta = @"CREATE TABLE vehiculos (matricula TEXT, marca TEXT, modelo TEXT, color TEXT)";
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Agrega vehiculos de prueba a la BBDD
        /// </summary>
        private void VehiculosEjemplo()
        {
            string[] matriculas = new string[10] { "8165DYL", "4367YVZ", "9646ABC", "9647YGH", "1206LNM", "8564KHG", "6472NBC", "8474KDA", "6587YTU", "9867HJK" };
            string[] marcas = new string[10] { "Ford", "Renault", "Audi", "BMW", "Chevrolet", "Citroen", "Fiat", "Audi", "BMW", "Chevrolet" };
            string[] modelos = new string[10] { "Kuga", "Coupé", "A3", "320D", "Camaro", "C4", "Punto", "S8", "330e", "Cruze" };
            string[] colores = new string[10] { "Negro", "Azul", "Blanco", "Perla", "Amarillo", "Gris", "Azul", "Negro", "Gris", "Rojo" };
            conexion.Open();
            for (int i = 0; i < 10; i++)
            {
                string consulta = string.Format("INSERT INTO vehiculos VALUES ('{0}', '{1}', '{2}', '{3}')", matriculas[i], marcas[i], modelos[i], colores[i]);
                SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
                comando.ExecuteNonQuery();
            }
            conexion.Close();
        }

        /// <summary>
        /// Agrega un vehiculo a la lista y BBDD
        /// </summary>
        /// <param name="vehiculo"></param>
        public void Agregar(Vehiculo vehiculo)
        {
            ListaVehiculos.Add(vehiculo);
            conexion.Open();
            string consulta = string.Format("INSERT INTO vehiculos VALUES ('{0}', '{1}', '{2}', '{3}')", vehiculo.matricula, vehiculo.marca, vehiculo.modelo, vehiculo.color);
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Actualiza un registro en la lista y BBDD
        /// </summary>
        /// <param name="vehiculo"></param>
        /// <param name="registro"></param>
        public void Actualizar(Vehiculo vehiculo, int registro)
        {
            string matricula = vehiculo.matricula;
            string marca = vehiculo.marca;
            string modelo = vehiculo.modelo;
            string color = vehiculo.color;

            string matriculaOriginal = ListaVehiculos[registro - 1].matricula;

            conexion.Open();
            string consulta = string.Format(@"UPDATE vehiculos SET matricula = '{0}', marca = '{1}', modelo = '{2}', color = '{3}' WHERE matricula = '{4}'", matricula,marca,modelo,color,matriculaOriginal);
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();

            Vehiculo automovil;
            for(int i=0;i<ListaVehiculos.Count;i++)
            {
                automovil = ListaVehiculos[i];
                if (automovil.matricula==matriculaOriginal)
                {
                    automovil.matricula = matricula;
                    automovil.marca = marca;
                    automovil.modelo = modelo;
                    automovil.color = color;
                    ListaVehiculos[i] = automovil;
                    break;
                }
            }
        }

        /// <summary>
        /// Devuelve el vehiculo mediante el numero de registro
        /// </summary>
        /// <param name="registro"></param>
        /// <returns></returns>
        public Vehiculo IraRegistro(int registro)
        {
            return ListaVehiculos[registro-1];
        }

        /// <summary>
        /// Devuelve un vehiculo por su matrícula
        /// </summary>
        /// <param name="matricula"></param>
        /// <returns></returns>
        public string[] Buscar(string matricula)
        {
            Vehiculo vehiculo;
            for (int i=0;i<ListaVehiculos.Count;i++)
            {
                vehiculo = ListaVehiculos[i];
                if (vehiculo.matricula == matricula)
                {
                    string[] datosVehiculo = new string[5];
                    datosVehiculo[0] = vehiculo.matricula;
                    datosVehiculo[1] = vehiculo.marca;
                    datosVehiculo[2] = vehiculo.modelo;
                    datosVehiculo[3] = vehiculo.color;
                    datosVehiculo[4] = (i + 1).ToString(); //Guardamos la posicion del registro
                    return datosVehiculo;
                }
            }
            return null;
        }

        /// <summary>
        /// Elimina un vehiculo de la lista y BBDD
        /// </summary>
        /// <param name="matricula"></param>
        public void Eliminar(string matricula)
        {
            Vehiculo vehiculo;
            for (int i = 0; i < ListaVehiculos.Count; i++)
            {
                vehiculo = ListaVehiculos[i];
                if (vehiculo.matricula == matricula)
                {
                    ListaVehiculos.Remove(vehiculo);
                }
            }
            string consulta = string.Format("DELETE FROM vehiculos WHERE matricula = '{0}'", matricula);
            conexion.Open();
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            comando.ExecuteNonQuery();
            conexion.Close();
        }

        /// <summary>
        /// Escribe en el archivo el informe
        /// </summary>
        /// <param name="informe"></param>
        public void EscribirInforme(TextWriter informe)
        {
            Vehiculo vehiculo;
            int id = 0;
            informe.WriteLine("{0} {1} {2} {3} {4}", "ID".PadRight(4), "Matrícula".PadRight(15), "Marca".PadRight(20), "Modelo".PadRight(40), "Color".PadRight(15));
            string consulta = @"SELECT * FROM vehiculos";
            conexion.Open();
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            SQLiteDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                id++;
                vehiculo.matricula = lector["matricula"].ToString();
                vehiculo.marca = lector["marca"].ToString();
                vehiculo.modelo = lector["modelo"].ToString();
                vehiculo.color = lector["color"].ToString();
                informe.WriteLine("{0} {1} {2} {3} {4}", id.ToString().PadRight(4), vehiculo.matricula.PadRight(15), vehiculo.marca.PadRight(20), vehiculo.modelo.PadRight(40), vehiculo.color.PadRight(15));
            }
            conexion.Close();
        }

        /// <summary>
        /// Lee la BBDD y guarda los registros en una lista
        /// </summary>
        private void LeerBaseDatos()
        {
            conexion.Open();
            Vehiculo vehiculo;
            string consulta = @"SELECT * FROM vehiculos";
            SQLiteCommand comando = new SQLiteCommand(consulta, conexion);
            SQLiteDataReader lector = comando.ExecuteReader();
            while (lector.Read())
            {
                vehiculo.matricula = lector["matricula"].ToString();
                vehiculo.marca = lector["marca"].ToString();
                vehiculo.modelo = lector["modelo"].ToString();
                vehiculo.color = lector["color"].ToString();
                ListaVehiculos.Add(vehiculo);
            }
            conexion.Close();
        }

        #endregion

        #region Propiedades
        /// <summary>
        /// Devuelve el total de vehiculos
        /// </summary>
        public int TotalRegistros
        {
            get
            {
                return ListaVehiculos.Count;
            }
        }
        #endregion
    }
}
